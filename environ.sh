#!/bin/bash

export HACKPATH="$LANG_PATH/dist"

################################################################################

ronin_require_hack () {
    echo hello world >/dev/null
}

ronin_include_hack () {
    motd_text "    -> Hack    : "$HACKPATH
}

################################################################################

ronin_setup_hack () {
    echo hello world >/dev/null
}

